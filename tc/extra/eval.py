import tacticstuff as ts
import subprocess
import traceback
import asyncio
import shlex
import re

input_re = re.compile('.*```(.*)\n([\s\S]*)```')

async_exec = '''
async def f(*, {}):
  try:
{}
  except Exception as e:
    return_vals['exception'] = e

  end_event.set()

return_vals['task'] = asyncio.ensure_future(f({}), loop=loop)
'''

persist = {}

def_eval = eval

def what_is(s):
    if s.lower() in ['love', 'love?']:
        return 'baby don\'t hurt me!'
    else:
        return 'ask again later.'


async def eval(*, env={}, ctx, code, output_filter=lambda x: x):
    out_msg = f'Output: Processing...'
    msg = await ctx.say(out_msg)

    output_lang = ''
    output_lang_nice = 'Output'
    output = ts.classes.Lines()

    end_event = asyncio.Event()

    return_vals = {
        'exception': None
    }

    env = {
        'ctx': ctx,
        'print': output.append,
        'cprint': print,
        'loop': asyncio.get_event_loop(),
        'end_event': end_event,
        'return_vals': return_vals,
        'persist': persist,
        'what_is': what_is,
        **env
    }

    env['env'] = env

    match = input_re.match(ctx.message.content)
    lang = match.group(1)
    code = match.group(2)

    try:

        if lang in ['', 'py', 'python']:
            output_lang = 'py'
            try:
                output_lang_nice = 'Python Eval Output'
                res = def_eval(code, None, env)
                if asyncio.iscoroutine(res):
                    res = await res

                output.append(res)

            except SyntaxError:
                output_lang_nice = 'Python Exec Output'
                exec_code = async_exec.format(
                    ', '.join(env),
                    '\n'.join(map(lambda l: '    ' + l, code.split('\n'))),
                    ', '.join([ f'{k}={k}' for k in env ])
                )
                exec(exec_code, None, env)

                await asyncio.wait_for(end_event.wait(), 240)
                if return_vals['exception'] is not None:
                    raise return_vals['exception']

        elif lang in ['sh', 'bash', 'shell', 'fish']:
            output_lang = 'shell'
            output_lang_nice = 'Shell Output'

            if lang == 'fish':
                code = f'fish -c "{cmd}"'

            proc = subprocess.Popen(shlex.split(code), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            res = proc.stdout.read()
            output.append(res.decode('utf-8'))

    except:
        output.append(traceback.format_exc())
        output_lang = ''
        output_lang_nice = 'Traceback'

    out = str(output)
    out = output_filter(out)

    # res = (res if out == '' else out)
    out_msg = f'{output_lang_nice}: '
    if out == '':
        out_msg += 'Silence...'
    else:
        out_msg += f'```{output_lang}\n{out}```'

    await msg.edit(content=out_msg)
