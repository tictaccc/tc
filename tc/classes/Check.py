
class Check:
    def __init__(self, func, err=None, name=None):
        self.func = func
        self.name = (func.__name__ if name is None else name)
        self._err_ = err

    def err(self, ctx):
        if isinstance(self._err_, str):
            return self._err_
        elif callable(self._err_):
            return self._err_(ctx)
        else:
            return f"Failed check `{self.name}`"

    def __call__(self, *args, **kwargs):
        return self.func(*args, **kwargs)
