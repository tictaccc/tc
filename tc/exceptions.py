class TcBaseException(BaseException):

    def __str__(self):
        return "<tc.exceptions.{0.__name__} {1}".format(
            self,
            [   f"{k}='{self[k]}'" for k in filter(
                    lambda a: not a.startswith('__'),
                    dir(self)
                )
            ]
        )

class LoadException(BaseException):
    pass


class ExtensionLoadError(BaseException):

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "<tc.exceptions.ExtensionLoadError message='{}'>".format(self.msg)


class InvalidArguemnts(BaseException):
    pass


class UserPermissionError(BaseException):

    def __init__(self, permission):
        self.permission = permission

    def __str__(self):
        return "<tc.exceptions.UserPermissionError missing_permission='{}'>".format(self.permission)


class NsfwError(BaseException):

    def __init__(self):
        # self.command = command
        pass

    def __str__(self):
        return '<tc.exceptions.NsfwError>'


class ClientPermissionError(TcBaseException):
    """
    This exception is for when a command is
    set to require a server permission
    but lacks it when the command pre-run
    checks are completed.
    """
    def __init__(self, permission):
        self.permission = permission

    def __str__(self):
        return "<tc.exceptions.BotPermissionError missing_permission='{}'>".format(self.permission)

class CheckError(BaseException):
    def __init__(self, check):
        self.check = check

    def __str__(self):
        return "<tc.exceptions.CheckError failed_check='{}'>".format(self.check.__name__)

class ArgException(TcBaseException):
    def __init__(self, msg):
        self.msg = msg    
